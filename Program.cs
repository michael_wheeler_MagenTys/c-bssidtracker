﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Devices.WiFi;

namespace Windows.Devices.WiFi
{
    class WiFiScanner
    {
        public WiFiAdapter WiFiAdapter { get; private set; }
        public Dictionary WiFiInformation { get; private set; }

        private async Task InitializeFirstAdapter()
        {
            var access = await WiFiAdapter.RequestAccessAsync();
            if (access != WiFiAccessStatus.Allowed)
            {
                throw new Exception("WiFiAccessStatus not allowed");
            }
            else
            {
                var wifiAdapterResults =
                    await DeviceInformation.FindAllAsync(WiFiAdapter.GetDeviceSelector());
            }
            if (wifiAdapterResults.Count >= 1)
            {
                this.WiFiAdapter = await WiFiAdapter.FromIdAsync(wifiAdapterResults[0].Id);
            }
            else
            {
                throw new Exception("WiFi Adapter not found.");
            }
        }
        public async Task ScanForNetworks()
        {
            if (this.WiFiAdapter != null)
            {
                await this.WiFiAdapter.ScanAsync();
            }
        }
        public async Task RetrieveNetworkInformation()
        {
            foreach (var availableNetwork in report.AvailableNetworks)
            {
                WiFiSignal wifiSignal = new WiFiSignal()
                {
                    BSSID = availableNetwork.Bssid,
                    Signal = availableNetwork.Signal,
                    WiFiInformation.Add(BSSID, Signal)
                };
            }
        }
    }
}
